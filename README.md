# TimeCube

This is yet another clock, following on from the previous CryptiClock
and SmartClock projects.  This project will use an 8x8 simple LED module
for the display with a MAX7219 driving the display and an ESP8266 12-F board
as the brain.  The usual components sensing orientation and ambient light
levels will be included.

The display is 32x32mm and the final case using 2mm thick acrylic is 
a 36mm cube.  The firmware will use the CryptiClock display modes of
Binary/Cistercian/Die.

## Clock display

The display consists of an 8 by 8 square of pixels.  The 3 by 3 square of 
pixels in each of the four corners display 4 digits used to show the time
and date data.  There are three builtin methods of showing these digits:

* Binary
* Cistercian
* Die

The "die" digits are just the normal die face values extended up to 9:

![die digits](die_digits.png)

Note that a TimeCube display may not have the same colour or shape of
pixels.

The binary and Cistercian methods are left unexplained as a challenge to the
user!  As a hint:

https://en.wikipedia.org/wiki/Cistercian_numerals

## Time display

The four time digits are displayed with the hours in the top row and the
minutes in the bottom row.  The display below is showing the time 12:34:

![time display](time_display.png)

Note that the display is showing 4 digits in the centre of the display.
This means the time is being shown.  The year, date and day of the week
displays show different numbers of pixels in the centre.

Also note the pixel hightlighted in red between the minute digits.  The
pixel moves left and right each second, moving upwards as the minute 
elapses.  This is just so the display doesn't look frozen, plus giving
an indication of where in the minute the time is.

## Year display

If you configure the date display to be on (see configuration later),
then around the middle of each minute the year, date and the day of
the week are displayed, each lasting for about 4 seconds.

The year display shows the 4 digit year with a single pixel turned on in
the middle.  The display below is showing the year 2568 of the Buddhist
calendar:

![year display](year_display.png)

The date is shown next, with the month number on the top row and the day
number on the bottom row.  The centre has two pixels turned on.  The display
below is showing a date of January 9:

![date display](date_display.png)

Finally, the day of the week is displayed.  This is a single digit in
the top-right position, 1 to 7.  Three of the centre pixels are turned on.

There may or may not be a single displayed pixel in the top row to the
left of the digit.  If this pixel is showing it means the first day of the
week is a Sunday.  The first day can be configured to be Sunday or Monday
(see later).  The display below is showing the day of the week as Thursday:

![dow display](dow_display.png)

## Configuration

When first powered up the clock should display the "configuring" display,
possibly after a delay if a previous WiFi configuration remains.  If you
wish to change some configuration values in a previously configured clock
you can force configuration by applying power while pressing the "Config"
button.  This will bring up the "configuring" display:

![configure](configure_display.png)

Now connnect your phone or tablet to the WiFi access point *TimeCube x.y*:

![WiFi](wifi_display.jpg)

and you should see the configuration page:

![configpage](config_english.jpg)

Select the Access Point you want TimeCube to use from the list in the *SSID*
field.  If the AP you want isn't in the list move the TimeCube closer to the 
AP you want to use and again power down/up while holding the "Configure" button
down.

The AP names in the drop-down list should be ordered by signal strength,
strongest at the top.

Once the AP is selected enter a *Password* if one is required to access the AP.

The next field allows you to set the language for the configuration page to
English or Thai.  Any change here only takes effect after a save and reboot.

The *Timezone* field is next, but you don't need to set it.  TimeCube should
guess your timezone from your network's public IP address.  If that doesn't
work, or you want a different timezone from the one you are currently in, enter
your desired timezone.  Timezones have the form *Asia/Bangkok*.  If you don't
know your timezone identifier go to *https://worldtimeapi.org/api/timezone.txt*
and look for your timezone in that page.

In the next field you can select the type of display you want.  You can choose between:

* Binary
* Cistercian
* Die

Next is the *Reboot hour*.  TimeCube reboots once per day at a configurable
hour.  This reboot checks for the time including any daylight saving in effect.
So you need to set the boot hour to a short time, say one hour, after the hour
daylight savings changes in your locality.

Next, choose the *Calendar* you want to use.

The next two fields control the date display.  You can turn that display off in
the *Show date* field.  If on, the clock will show the year, then date, then
day of the week around the middle of each minute.

The *First DoW* field allows you to set Sunday or Monday as the first day of the
week.

The clock will sense the ambient light and dim the display if the room is dark.
The *Brightness* field allows you to vary the overall brightness of the display
a little.

The last two fields (*Debug* and *OTA*) are not important to the end user.

Pressing the "Save" button saves any changes you made and reboots the clock.

### Problems

If the clock shows garbage on the display when powered up, just remove and reapply
the power.

If you mess up a configuration attempt don't press the save button, just remove
and reapply the power, pressing the "Config" button again if necessary.

If you accidentally select *OTA* and then "Save", the clock will show a
special OTA display and wait for an "Over The Air" upload:

![ota](ota_display.png)

Just remove power and reapply.  Any other changes you made in the other fields
will have been saved.

## Status

The basic clock is finished and in a nice laser-cut acrylic case.

The DXF cut file for the case was designed in KiCad and the file is
*case/TimeCube_case_glue.dxf*.

