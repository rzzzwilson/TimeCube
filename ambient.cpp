//********************************************
// A library to sense the ambient light level.
//********************************************

#include "TimeCube.h"
#include "utility.h"
#include "ambient.h"
#include "display.h"


// save the pin number that we read an analog value from
static int AmbientPin = 0;

//---------------------------------------------
// Initialize the sensor.
//
//     pin  the GPIO pin to read ADC voltage from LDR
//---------------------------------------------

void ambient_begin(int pin)
{
  AmbientPin = pin;
  pinMode(pin, INPUT);
}

//---------------------------------------------
// Get the current light level.
//
// Returns an integer in the range [0..8].
// 0 means minimum light, 8 is max brightness
//---------------------------------------------

int ambient_read(void) 
{
  // constrain measurement to usable range
  int ambient_raw = constrain(analogRead(AmbientPin), 50, 1000);
  
  // map to the required [0..8]
  return map(ambient_raw, 50, 1000, 0, 8);
}

//---------------------------------------------
// Get the current RAW light level.
//
// Returns an integer in the range [0..1023].
// Debug routine to help getting light settings right.
//---------------------------------------------

int ambient_raw(void) 
{
  // return voltage int from LDR
  return analogRead(AmbientPin);
}
