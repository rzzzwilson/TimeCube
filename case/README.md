# TimeCube Case

The acrylic case will be similar in design to the other clocks, but
will be a 36x36mm clean (no overhangs) near cube.

## Getting DXF file

Just execute this command::

    pcb2dxf.py case.kicad_pcb case.dxf
