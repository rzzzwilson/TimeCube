//********************************************
// Interface to a simple library to handle an
// 8x8 LED display using a MAX7219.
//
// Same-ish API for all clocks.
//********************************************

#include <SPI.h>

#include "TimeCube.h"
#include "display.h"
#include "utility.h"
#include "pins.h"


//--------------------------------------------------------
// Last displayed bootstage number.
// Needed by the disp_flash_boot_stage() function.
//--------------------------------------------------------

int BootStage = 0;

// SS pin on controller, set in disp_begin()
int SSPin = 0;

// define 7219 register addresses for setup
const byte DECODE_MODE = 0x09;  // 0x00 = No decode for digits
const byte INTENSITY = 0x0A;    // 0x08 = mid level. Range is 0x00 to 0x0F
const byte SCAN_LIMIT = 0x0B;   // 0x08 for all 8 columns
const byte SHUTDOWN = 0x0C;     // 0x00 = shutdown, 0x01 = normal
const byte DISPLAY_TEST = 0x0F; // 0x00 = normal, 0x01 = display test mode all on full

// enum for the four digit places
enum DigitPlace {topleft=0, topright, bottomleft, bottomright};

// table giving x/y offsets for the bottomleft LED in each numeric 3x3 position
byte DigitXYOffsets[][4] = {{0,5},  // topleft
                            {5,5},  // topright
                            {0,0},  // bottomleft
                            {5,0}}; // bottomright

typedef struct
{
  const byte rows[3];
} Digit;

typedef Digit *PDigit;

// data describing BINARY digits (offset from bottomleft LED)
// each digit described by 3 bytes, the rightmost 3 bits is data, from bottom up
Digit BinDigitShape[] = {{0x00, 0x00, 0x00}, // 0
                         {0x01, 0x00, 0x00}, // 1
                         {0x04, 0x00, 0x00}, // 2
                         {0x05, 0x00, 0x00}, // 3
                         {0x00, 0x00, 0x01}, // 4
                         {0x01, 0x00, 0x01}, // 5
                         {0x04, 0x00, 0x01}, // 6
                         {0x05, 0x00, 0x01}, // 7
                         {0x00, 0x00, 0x04}, // 8
                         {0x01, 0x00, 0x04}, // 9
                        };

// data describing CYSTERCIAN digits (offset from bottomleft LED)
// each digit described by 3 bytes, the rightmost 3 bits is data, from bottom up
// the digits are different when in different corners, so  CysDigitShapeTL, etc.
Digit CysDigitShapeTR[] = {{0x00, 0x00, 0x00},
                           {0x07, 0x00, 0x00},
                           {0x00, 0x00, 0x07},
                           {0x04, 0x02, 0x01},
                           {0x01, 0x02, 0x04},
                           {0x07, 0x02, 0x04},
                           {0x01, 0x01, 0x01},
                           {0x07, 0x01, 0x01},
                           {0x01, 0x01, 0x07},
                           {0x07, 0x01, 0x07},
                          };

Digit CysDigitShapeTL[] = {{0x00, 0x00, 0x00},
                           {0x07, 0x00, 0x00},
                           {0x00, 0x00, 0x07},
                           {0x01, 0x02, 0x04},
                           {0x04, 0x02, 0x01},
                           {0x07, 0x02, 0x01},
                           {0x04, 0x04, 0x04},
                           {0x07, 0x04, 0x04},
                           {0x04, 0x04, 0x07},
                           {0x07, 0x04, 0x07},
                          };

Digit CysDigitShapeBL[] = {{0x00, 0x00, 0x00},
                           {0x00, 0x00, 0x07},
                           {0x07, 0x00, 0x00},
                           {0x04, 0x02, 0x01},
                           {0x01, 0x02, 0x04},
                           {0x01, 0x02, 0x07},
                           {0x04, 0x04, 0x04},
                           {0x04, 0x04, 0x07},
                           {0x07, 0x04, 0x04},
                           {0x07, 0x04, 0x07},
                          };

Digit CysDigitShapeBR[] = {{0x00, 0x00, 0x00},
                           {0x00, 0x00, 0x07},
                           {0x07, 0x00, 0x00},
                           {0x01, 0x02, 0x04},
                           {0x04, 0x02, 0x01},
                           {0x04, 0x02, 0x07},
                           {0x01, 0x01, 0x01},
                           {0x01, 0x01, 0x07},
                           {0x07, 0x01, 0x01},
                           {0x07, 0x01, 0x07},
                          };

// data describing DIE digits (offset from bottomleft LED)
// each digit described by 3 bytes, the rightmost 3 bits is data, from bottom up
Digit DieDigitShape[] = {{0x00, 0x00, 0x00}, // 0
                         {0x00, 0x02, 0x00}, // 1
                         {0x01, 0x00, 0x04}, // 2
                         {0x01, 0x02, 0x04}, // 3
                         {0x05, 0x00, 0x05}, // 4
                         {0x05, 0x02, 0x05}, // 5
                         {0x07, 0x00, 0x07}, // 6
                         {0x07, 0x02, 0x07}, // 7
                         {0x07, 0x05, 0x07}, // 8
                         {0x07, 0x07, 0x07}, // 9
                        };

Digit *TLDigits = &CysDigitShapeTL[0];
Digit *TRDigits = &CysDigitShapeTR[0];
Digit *BLDigits = &CysDigitShapeBL[0];
Digit *BRDigits = &CysDigitShapeBR[0];

// in-memory store of column data
byte columns[] = {0, 0, 0, 0, 0, 0, 0, 0};

//--------------------------------------------------------
// Display orientation
//--------------------------------------------------------

static int Orientation = 0;

//---------------------------------------------
// Send SPI "data" to MAX7219 address "addr".
//---------------------------------------------

static void spi_send(byte addr, byte data)
{
  digitalWrite(SSPin, LOW);
  SPI.transfer(addr);
  SPI.transfer(data);
  digitalWrite(SSPin, HIGH);
}

//--------------------------------------------------------
// Flash the stage number display of the boot.
//
// Flashes the LED number stored in the BootStage global.
// disp_boot_stage() sets BootStage.
//--------------------------------------------------------

void disp_flash_boot_stage(void)
{
  for (int i=0; i < 2; ++i)
  {
    disp_clear();
    disp_show();
    delay(50);
    disp_boot_stage(BootStage);
    disp_show();
    delay(100);
  }
}

//---------------------------------------------
// Initialize the display.
//---------------------------------------------

void disp_begin(int ss)
{
  SSPin = ss;
  
  pinMode(SSPin, OUTPUT);  
  digitalWrite(SSPin, HIGH);

  // turn on SPI port, configure display
  SPI.begin();
  spi_send(DECODE_MODE, 0x00);    // don't decode digits
  spi_send(DISPLAY_TEST, 0x00);   // non-test mode
  spi_send(INTENSITY, 1);         // set minimum intensity
  spi_send(SCAN_LIMIT, 0x07);     // set display for all 8 columns of LEDs
  spi_send(SHUTDOWN, 0x01);       // display to normal mode
}

//---------------------------------------------
// Clear the display.
//---------------------------------------------

void disp_clear(int value)
{
  for (int x = 0; x < 8; ++x)
  {
    for (int y = 0; y < 8; ++y)
    {
      disp_set(x, y, value);
    }
  }
  
  disp_show();
}

//---------------------------------------------
// Draw a digit on the display.
//     place  where to draw the 3x3 digit (topleft, etc)
//     value  value of digit to show
//
// "value" is an index into a data table containing
// offset values showing where to turn on an LED.
//---------------------------------------------

void draw_digit(DigitPlace place, byte value)
{
  PDigit dxdy = NULL;
  
  switch (place)
  {
    case topleft:
      dxdy = &TLDigits[value];
      break;
    case topright:
      dxdy = &TRDigits[value];
      break;
    case bottomleft:
      dxdy = &BLDigits[value];
      break;
    case bottomright:
      dxdy = &BRDigits[value];
      break;
    default:
      abort("Bad switch value (%d) at line %d, %s\n", place, __LINE__, __FILE__);
  }
  
  byte *offset = DigitXYOffsets[place];
  int x_offset = offset[0];
  int y_offset = offset[1];
  
  for (int i = 0; i < 3; ++i)
  {
    byte row = dxdy->rows[i];
    byte mask = 0x04;
    int count = 0;
    
    while (mask)
    {
      disp_set(x_offset + count, y_offset + i, (row & mask));
      mask >>= 1;
      count += 1;
    }
  }
}

//--------------------------------------------------------
// Twiddle the "working" indicator on the screen.
//
// This twiddle tries to give a rough indication of the seconds.
// The three centre-bottom two columns are split.  The bottom
// two indicate the seconds are in the first 20 seconds in the
// minute.  The next row up for the middle 20 seconds and the
// top two for the last 20 seconds.
//--------------------------------------------------------

static void twiddle(byte seconds)
{
  // previous twiddle position
  static byte prev_twiddle_col = 255;
  static byte prev_twiddle_row = 255;

  // turn off previous twiddle dot if set
  if (prev_twiddle_col != 255)
  {
    disp_set(prev_twiddle_col, prev_twiddle_row, 0);
  }

  // determine row to twiddle and seconds remaining
  byte row = seconds / 20;    // determine the row to use

  // calculate position we are to use, left or right column
  byte col = (seconds % 2) ? 4 : 3;

  // set the twiddle dot
  disp_set(col, row, 1);

  prev_twiddle_col = col;
  prev_twiddle_row = row;
}

//---------------------------------------------
// Show a time on the display.
//     hours, minutes, seconds  the time to show
//
// Display a "seconds" twiddle in the lower middle columns.
//---------------------------------------------

void disp_time(int hours, int minutes, int seconds)
{
  draw_digit(topleft, hours / 10);
  draw_digit(topright, hours % 10);
  draw_digit(bottomleft, minutes / 10);
  draw_digit(bottomright, minutes % 10);

  twiddle(seconds);
  
  disp_show();
}

//---------------------------------------------
// Show a date on the display.
//     month, day  the date to show
//
// Leading zeros on day and month are suppressed.
//---------------------------------------------

void disp_date(int month, int day)
{
  draw_digit(topleft, month / 10);
  draw_digit(topright, month % 10);
  draw_digit(bottomleft, day / 10);
  draw_digit(bottomright, day % 10);
  
  disp_show();
}

//---------------------------------------------
// Show the day of the week on the display.
//     day        the date to show
//     sun_first  true if Sunday is first, else Monday
//
// Show digit 4 DP if Sunday is first DoW.
//---------------------------------------------

void disp_dow(int day, bool sun_first)
{
  byte day_digit = day + 1; // assume Sunday is first DoW

  if (!sun_first)
  {
    // Monday is first, raw Sunday (0) is 7, rest unchanged
    day_digit = (day == 0) ? 7 : day;
  }

  draw_digit(topleft, 0);
  draw_digit(topright, day_digit);
  draw_digit(bottomleft, 0);
  draw_digit(bottomright, 0);
  disp_set(3, 7, sun_first);    // pixel showing Sunday is first DoW
  
  disp_show();
}

//---------------------------------------------
// Show a year on the display.
//     year  the date to show
//---------------------------------------------

void disp_year(int year)
{
  draw_digit(topleft, (year / 1000) % 10);
  draw_digit(topright, (year / 100) % 10);
  draw_digit(bottomleft, (year / 10) % 10);
  draw_digit(bottomright, year % 10);
  
  disp_show();
}

//---------------------------------------------
// A function to show boot "state".
//     state  the number of the state to show [1..4]
//     pause  minimum display time
//
// Turns on LEDs in chevrons from each corner.  As
// the stage number increases, the chevrons move
// toward the centre of the display.
//---------------------------------------------

void disp_boot_stage(int stage, int pause)
{
  // remember current boot stage in case we have to flash
  BootStage = stage;

  disp_clear();

  stage -= 1;   // map [1..4] to [0..3]

  disp_set(stage, stage, 1);
  disp_set(7-stage, stage, 1);
  disp_set(stage, 7-stage, 1);
  disp_set(7-stage, 7-stage, 1);

  disp_show();
  delay(pause);
}

//---------------------------------------------
// Set the display orientation.
//     orient  the orientation to use
//
// Ignore Z-dominant results.
//---------------------------------------------

void disp_orientation(int orient)
{
  if (orient < 4)
    Orientation = orient;
}

//*********************************************
// Various functions to draw status symbols.
//---------------------------------------------
// Show the clock is in CONFIG mode.
//---------------------------------------------

void status_config(void)
{
  for (int i=0; i<8; ++i)
  {
    disp_set(i, i, 1);
    disp_set(7-i, i, 1);
  }
}

//---------------------------------------------
// Show the clock is waiting for an OTA upload.
//---------------------------------------------

void status_ota(void)
{
  disp_set(0, 7, 1);	// "O"
  disp_set(1, 7, 1);
  disp_set(2, 7, 1);
  disp_set(0, 6, 1);
  disp_set(2, 6, 1);
  disp_set(0, 5, 1);
  disp_set(1, 5, 1);
  disp_set(2, 5, 1);
  
  disp_set(2, 4, 1);	// "T"
  disp_set(3, 4, 1);
  disp_set(4, 4, 1);
  disp_set(5, 4, 1);
  disp_set(3, 3, 1);
  disp_set(4, 3, 1);
  disp_set(3, 2, 1);
  disp_set(4, 2, 1);
  
  disp_set(5, 2, 1);	// "A"
  disp_set(6, 2, 1);
  disp_set(7, 2, 1);
  disp_set(5, 1, 1);
  disp_set(6, 1, 1);
  disp_set(7, 1, 1);
  disp_set(5, 0, 1);
  disp_set(7, 0, 1);
}

//---------------------------------------------
// Show one to four year/date/dow/time status centre LEDs.
//---------------------------------------------

void status_date(int num)
{
  if (num-- > 0)
    disp_set(3, 4, 1);
  if (num-- > 0)
    disp_set(4, 4, 1);
  if (num-- > 0)
    disp_set(4, 3, 1);
  if (num-- > 0)
    disp_set(3, 3, 1);
}

//---------------------------------------------
// Show the clock got a 404 on internet.
//---------------------------------------------

void status_404(void)
{
  disp_set(0, 7, 1);
  disp_set(2, 7, 1);
  disp_set(0, 5, 1);
  disp_set(2, 5, 1);
  disp_set(5, 2, 1);
  disp_set(7, 2, 1);
  disp_set(5, 0, 1);
  disp_set(7, 0, 1);
}

//---------------------------------------------
// Show an abort on the display.
//---------------------------------------------

void status_abort(void)
{
  disp_set(1, 5, 1);
  disp_set(2, 6, 1);
  disp_set(3, 7, 1);
  disp_set(4, 7, 1);
  disp_set(5, 6, 1);
  disp_set(6, 5, 1);
  disp_set(5, 4, 1);
  disp_set(4, 3, 1);
  disp_set(3, 2, 1);
  disp_set(3, 0, 1);
}

//---------------------------------------------
// Show some sort of status on the display.
//     status  enum value of the type of status to display
//---------------------------------------------

void disp_status(Status status)
{
  disp_clear();
  
  switch (status)
  {
    case STATUS_CONFIG:
      status_config();
      break;
    case STATUS_OTA:
      status_ota();
      break;     
    case STATUS_YEAR:
      status_date(1);
      break;
    case STATUS_DATE:
      status_date(2);
      break;
    case STATUS_DOW:
      status_date(3);
      break;
    case STATUS_CLOCK:
      status_date(4);
      break;
    case STATUS_404:
      status_404();
      break;
    case STATUS_ABORT:
      status_abort();
      break;
  }

  disp_show();
}

//--------------------------------------------------------
// disp_flash_status() - Flash a quick error status on display.
//     repeat  number of times to flash
//     first   first status in flash sequence
//     second  second status in flash sequence
//--------------------------------------------------------

void disp_flash_status(int repeat, Status first, Status second)
{
    for (int i = 0; i < repeat; ++i)
    {
      disp_status(first);
      disp_show();
      delay(100);
      disp_status(second);
      disp_show();
      delay(100);
    }
}

//---------------------------------------------
// Flush any display changes to the hardware.
//---------------------------------------------

void disp_show(void)
{
  for (int row=1; row <= 8; ++row)
  {
    spi_send(row, columns[row-1]);
  }
}

//---------------------------------------------
// Set the display brightness.
//     bright  the brightness level [0..8]
//
// Actual setting is "bright" + CONFIG brightness [0..7].
//---------------------------------------------

void disp_brightness(int bright)
{
  int level = bright + ClockData.Brightness;

  debugf("disp_brightness: level=%d\n", level);

  spi_send(INTENSITY, level);
}

//---------------------------------------------
// Set the clockface to use.
//     face  the face to use (Binary, Cystercian or Die)
//---------------------------------------------

void disp_face(char face)
{
  switch (face)
  {
    case 'B': 
      TLDigits = &BinDigitShape[0];
      TRDigits = &BinDigitShape[0];
      BLDigits = &BinDigitShape[0];
      BRDigits = &BinDigitShape[0];
      break;
    case 'C': 
      TLDigits = &CysDigitShapeTL[0];
      TRDigits = &CysDigitShapeTR[0];
      BLDigits = &CysDigitShapeBL[0];
      BRDigits = &CysDigitShapeBR[0];
      break;
    default:  // if unrecognized, default to using DIE
      printf("disp_face: Unrecognized face '%c', using 'D'\n", face);
      [[fallthrough]];
    case 'D': 
      TLDigits = &DieDigitShape[0];
      TRDigits = &DieDigitShape[0];
      BLDigits = &DieDigitShape[0];
      BRDigits = &DieDigitShape[0];
      break;
  }
}

//---------------------------------------------
// Show the display in TEST mode, everything ON.
//     pause  the time in milliseconds to show the test
//---------------------------------------------

void disp_test(int pause)
{
  disp_clear(1);
  delay(pause);
  disp_clear(0);
}

//---------------------------------------------
// Set the value of the X,Y LED.
//     x, y   coordinates of LED
//     value  new value for LED, true or false
//
// The orientation is taken into account when setting bits.
//
// We just set the appropriate bit in "columns[]".
// The "disp_show()" function actually displays the LEDs.
//---------------------------------------------

void disp_set(int x, int y, bool value)
{
  byte col_data;
  byte col_inv_data;

  switch (Orientation)
  {
    case 0:
      col_data = 0x80 >> x;
      col_inv_data = ~col_data;
      if (value)
        columns[7-y] |= col_data;  
      else
        columns[7-y] &= col_inv_data;
      break;
    case 1:
      col_data = 0x80 >> (7-y);
      col_inv_data = ~col_data;
      if (value)
        columns[7-x] |= col_data;  
      else
        columns[7-x] &= col_inv_data;
      break;
    case 2:
      col_data = 0x80 >> (7-x);
      col_inv_data = ~col_data;
      if (value)
        columns[y] |= col_data;  
      else
        columns[y] &= col_inv_data;
      break;
    case 3:
      col_data = 0x80 >> y;
      col_inv_data = ~col_data;
      if (value)
        columns[x] |= col_data;  
      else
        columns[x] &= col_inv_data;
      break;
  }
}

//---------------------------------------------
// Show a debug number on the builtin LED.
//     num  the number to show
//---------------------------------------------

void disp_debug(int num)
{
  disp_clear();
  
  draw_digit(topleft, (num / 1000) % 10);
  draw_digit(topright, (num / 100) % 10);
  draw_digit(bottomleft, (num / 10) % 10);
  draw_digit(bottomright, num % 10);

  disp_show();
  delay(500);
}
