//********************************************
// Interface to a simple library to handle an 8x8 LED display
// using the MAX7219.  Same-ish API as the SmartClock API.
//********************************************

#ifndef _DISPLAY_H_
#define _DISPLAY_H_

void disp_set(int x, int y, bool value);
void disp_begin(int ss);
void disp_clear(int value=0);
void disp_show(void);
void disp_face(char face);
void disp_orientation(int orient);
void disp_brightness(int level);
void disp_status(Status stat);
void disp_time(int hours, int minutes, int seconds);
void disp_date(int month, int day);
void disp_dow(int day, bool sun_first);
void disp_year(int year);
void disp_flash_status(int repeat, Status first, Status second);
void disp_boot_stage(int stage, int pause=100);
void disp_test(int msec);
void disp_flash_boot_stage(void);
void disp_debug(int num);


#endif
