//********************************************
// A library to handle orientation data from an external sensor.
//********************************************

#include <Wire.h>
#include <MPU6050.h>

#include "TimeCube.h"
#include "utility.h"
#include "orientation.h"


//--------------------------------------------------------
// MPU6050 accelerometer
//--------------------------------------------------------

MPU6050 accelgyro;


//---------------------------------------------
// Initialize the sensor.
//
//     sca, sdl  comms pins
//---------------------------------------------

void orient_begin(int sca, int sdl)
{
  // prepare comms for accelerometer
  Wire.begin(sca, sdl);

  // initialize the accelerometer
  accelgyro.initialize();

  // wait a bit
  delay(100);
}

//---------------------------------------------
// Get the current orientation.
//
// Returns an integer with meaning:
//     0  X is UP
//     1  Y is UP
//     2  X is DOWN
//     3  Y is DOWN
//---------------------------------------------

int orient_read(void)
{
  int16_t ax, ay, az;
  byte result = 0;

  accelgyro.getAcceleration(&ax, &ay, &az);

  if (abs(ax) > abs(ay))
    result = (ax > 0) ? 1 : 3;
  else
    result = (ay > 0) ? 0 : 2;
  
  debugf("orient_read: ax=%d, ay-%d, az=%d, result=%d\n",
         ax, ay, az, result);

  return result;
}
