//********************************************
// Interface to simple debug printing functions.
//********************************************

#include "TimeCube.h"
#include "display.h"
#include "utility.h"

//--------------------------------------------------------
// Provide a "printf()"-like function, shortens code.
// Only prints if ClockData.Debug is non-zero.
//--------------------------------------------------------

void debugf(const char *fmt, ...)
{
  if (ClockData.Debug)
  {
    va_list ptr;
    char tmp_buff[512];
  
    memset(tmp_buff, 0, sizeof(tmp_buff));
    
    va_start(ptr, fmt);
    vsprintf(tmp_buff,fmt, ptr);
    va_end(ptr);
  
    Serial.print(tmp_buff);
  }
}

//--------------------------------------------------------
// Abort the program.
// Prints printf() style args, then loops forever.
//--------------------------------------------------------

void abort(const char *fmt, ...)
{
  va_list ptr;
  char tmp_buff[512];

  memset(tmp_buff, 0, sizeof(tmp_buff));
  
  va_start(ptr, fmt);
  vsprintf(tmp_buff,fmt, ptr);
  va_end(ptr);

  Serial.println(tmp_buff);

  while (1)
  {
    disp_status(STATUS_ABORT);
    delay(500);
    disp_clear();
    delay(100);
  }
}

//--------------------------------------------------------
// Provide a "printf()" function, shortens code.
// Always prints.
//--------------------------------------------------------

void printf(char *fmt, ...)
{
  va_list ptr;
  char tmp_buff[512];

  memset(tmp_buff, 0, sizeof(tmp_buff));
  
  va_start(ptr, fmt);
  vsprintf(tmp_buff,fmt, ptr);
  va_end(ptr);

  Serial.print(tmp_buff);
}
